#include "GameLayer.h"

void GameLayer::playerScore(int player)
{
	SimpleAudioEngine::getInstance()->playEffect("score.wav");

	ball->setVector(Vec2(0, 0));
	char score_buffer[10];
	if (player == 1) {
		player1Score++;
		player1ScoreLabel->setString(std::to_string(player1Score));
		ball->setNextPosition(Vec2(courtSize.width * 0.5, courtSize.height * 0.5 + 2 * ball->radius()));
	}
	else {
		player2Score++;
		player2ScoreLabel->setString(std::to_string(player2Score));
		ball->setNextPosition(Vec2(courtSize.width * 0.5, courtSize.height * 0.5 - 2 * ball->radius()));
	}
	//The players are moved to their original position and their _touch properties are cleared :
	player1->setPosition(Vec2(courtSize.width * 0.5, player1->radius() * 2));
	player2->setPosition(Vec2(courtSize.width * 0.5, courtSize.height - player1->radius() * 2));
	player1->setTouch(nullptr);
	player2->setTouch(nullptr);
}

GameLayer::GameLayer()
{
}


GameLayer::~GameLayer()
{
}

bool GameLayer::init()
{
	if (!Layer::init())
	{
		return false;
	}

	screenSize = Director::getInstance()->getWinSize();

	players = Vector<GameSprite*>(2);

	player1Score = 0;
	player2Score = 0;


	auto court = Sprite::create("court.png");
	court->setPosition(Vec2(screenSize.width * 0.5, screenSize.height * 0.5));
	this->addChild(court);

	courtSize = court->getContentSize();

	auto tableTop = Sprite::create("tableTop.png");
	tableTop->setPosition(Vec2(court->getContentSize().width * 0.5,
		court->getContentSize().height * 0.5));
	court->addChild(tableTop);

	player1 = GameSprite::gameSpriteWithFile("greenMallet.png");
	player1->setPosition(Vec2(screenSize.width * 0.5,
		player1->radius() * 2 + 20));
	players.pushBack(player1);
	this->addChild(player1);


	player2 = GameSprite::gameSpriteWithFile("redMallet.png");
	player2->setPosition(Vec2(screenSize.width * 0.5, screenSize.
		height - player1->radius() * 2));
	players.pushBack(player2);
	this->addChild(player2);


	ball = GameSprite::gameSpriteWithFile("puck.png");
	ball->setPosition(Vec2(screenSize.width * 0.5, screenSize.
		height * 0.5 - 2 * ball->radius()));
	this->addChild(ball);


	player1ScoreLabel = Label::createWithTTF("0",
		"fonts/arial.ttf", 50);
	player1ScoreLabel->setColor(Color3B(255, 50, 50));
	player1ScoreLabel->setPosition(Vec2(screenSize.width - 60,
		screenSize.height * 0.5 - 80));
	player1ScoreLabel->setRotation(90);
	this->addChild(player1ScoreLabel);

	player2ScoreLabel = Label::createWithTTF("0",
		"fonts/arial.ttf", 50);
	player2ScoreLabel->setColor(Color3B(255, 50, 50));
	player2ScoreLabel->setPosition(Vec2(screenSize.width - 60,
		screenSize.height * 0.5 + 80));
	player2ScoreLabel->setRotation(90);
	this->addChild(player2ScoreLabel);


	// ADDING EVENT LISTENERS
	auto listener = EventListenerTouchAllAtOnce::create();
	listener->onTouchesBegan =
		CC_CALLBACK_2(GameLayer::onTouchesBegan, this);
	listener->onTouchesMoved =
		CC_CALLBACK_2(GameLayer::onTouchesMoved, this);
	listener->onTouchesEnded =
		CC_CALLBACK_2(GameLayer::onTouchesEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,
		this);


	//CREATE MAIN LOOP
	this->scheduleUpdate();
	return true;

}

Scene * GameLayer::scene()
{
	auto scene = Scene::create();
	scene->addChild(GameLayer::create());
	return scene;
}

void GameLayer::onTouchesBegan(const std::vector<Touch*>& touches, Event * event)
{
	for (auto touch : touches) {
		if (touch != nullptr) {
			auto tap = touch->getLocation();
			for (auto player : players) {
				if (player->boundingBox().containsPoint(tap)) {
					player->setTouch(touch);
				}
			}
		}
	}
}

void GameLayer::onTouchesMoved(const std::vector<Touch*>& touches, Event * event)
{
	for (auto touch : touches) {
		if (touch != nullptr) {

			auto tap = touch->getLocation();

			for (auto player : players) {
				if (player->getTouch() != nullptr && player->getTouch() == touch) {
					Point nextPosition = tap;

					//prevent player to go out right side of screen
					if (nextPosition.x < player->radius() + 20)
						nextPosition.x = player->radius() + 20;

					//prevent player to go out left side of screen
					if (nextPosition.x > courtSize.width - player->radius())
						nextPosition.x = courtSize.width - player->radius();

					//prevent player to go out down side of screen
					if (nextPosition.y < player->radius() + 20)
						nextPosition.y = player->radius() + 20;

					//prevent player to go out up side of screen
					if (nextPosition.y > courtSize.height - player->radius())
						nextPosition.y = courtSize.height - player->radius();

					//keep player inside its court
					if (player->getPositionY() < courtSize.height * 0.5f + 10) {
						if (nextPosition.y > courtSize.height * 0.5 - player->radius() + 10) {
							nextPosition.y = courtSize.height * 0.5 - player->radius() + 10;
						}
					}
					else {
						if (nextPosition.y < courtSize.height * 0.5 + player->radius() + 10) {
							nextPosition.y = courtSize.height * 0.5 + player->radius() + 10;
						}
					}
					player->setNextPosition(nextPosition);
					player->setVector(Vec2(nextPosition.x - player->getPositionX(),
						nextPosition.y - player->getPositionY()));
				}
			}
		}
	}
}

void GameLayer::onTouchesEnded(const std::vector<Touch*>& touches, Event * event)
{
	for (auto touch : touches) {
		if (touch != nullptr) {

			auto tap = touch->getLocation();

			for (auto player : players) {
				if (player->getTouch() != nullptr && player->getTouch() == touch) 
				{
					//if touch ending belongs to this player, clear it
					player->setTouch(nullptr);
					player->setVector(Vec2(0, 0));
				}
			}
		}
	}
}

void GameLayer::update(float dt)
{

	auto ballNextPosition = ball->getNextPosition();
	auto ballVector = ball->getVector();

	ballVector *= 0.94f;
	ballNextPosition.x += ballVector.x;
	ballNextPosition.y += ballVector.y;

	float squared_radii = pow(player1->radius() + ball->radius(), 2);

	for (auto player : players) {

		auto playerNextPosition = player->getNextPosition();
		auto playerVector = player->getVector();

		float diffx = ballNextPosition.x - player->getPositionX();
		float diffy = ballNextPosition.y - player->getPositionY();
		float distance1 = pow(diffx, 2) + pow(diffy, 2);
		float distance2 = pow(ball->getPositionX() - playerNextPosition.x, 2) + pow(ball->getPositionY() - playerNextPosition.y, 2);

		if (distance1 <= squared_radii || distance2 <= squared_radii) {

			float mag_ball = pow(ballVector.x, 2) + pow(ballVector.y, 2);
			float mag_player = pow(playerVector.x, 2) + pow(playerVector.y, 2);
			float force = sqrt(mag_ball + mag_player);
			force = force / 2;

			float angle = atan2(diffy, diffx);
			ballVector.x = force * cos(angle);
			ballVector.y = (force * sin(angle));
			ballNextPosition.x = playerNextPosition.x + (player->radius() + ball->radius() + force) * cos(angle);
			ballNextPosition.y = playerNextPosition.y + (player->radius() + ball->radius() + force) * sin(angle);

			SimpleAudioEngine::getInstance()->playEffect("hit.mp3");

		}

		if (ballNextPosition.x < ball->radius() + 20) {
			ballNextPosition.x = ball->radius() + 20;
			ballVector.x *= -0.8f;
			SimpleAudioEngine::getInstance()->playEffect("hit.mp3");
		}

		if (ballNextPosition.x > courtSize.width - ball->radius()) {
			ballNextPosition.x = courtSize.width - ball->radius();
			ballVector.x *= -0.8f;
			SimpleAudioEngine::getInstance()->playEffect("hit.mp3");
		}

		if (ballNextPosition.y > courtSize.height - ball->radius()) {

			if (ball->getPosition().x < courtSize.width * 0.5f - GOAL_WIDTH * 0.5f
				|| ball->getPosition().x > courtSize.width * 0.5f + GOAL_WIDTH * 0.5f) {

				ballNextPosition.y = courtSize.height - ball->radius();
				ballVector.y *= -0.8f;
				SimpleAudioEngine::getInstance()->playEffect("hit.mp3");

			}
		}

		if (ballNextPosition.y < ball->radius() + 20) {

			if (ball->getPosition().x < courtSize.width * 0.5f - GOAL_WIDTH * 0.5f
				|| ball->getPosition().x > courtSize.width * 0.5f + GOAL_WIDTH * 0.5f) {

				ballNextPosition.y = ball->radius() + 20;
				ballVector.y *= -0.8f;
				SimpleAudioEngine::getInstance()->playEffect("hit.mp3");
			}
		}


		ball->setVector(ballVector);
		ball->setNextPosition(ballNextPosition);

		//check for goals!
		if (ballNextPosition.y < -ball->radius()) {
			this->playerScore(2);
		}

		if (ballNextPosition.y > courtSize.height + ball->radius())
		{
			this->playerScore(1);
		}

		player1->setPosition(player1->getNextPosition());
		player2->setPosition(player2->getNextPosition());
		ball->setPosition(ball->getNextPosition());

	}
}

#ifndef _GAMELAYER_H_
#define _GAMELAYER_H_

#define GOAL_WIDTH 150

#pragma once
#include "cocos2d.h"
#include "GameSprite.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;

class GameLayer : public Layer
{

	GameSprite* player1;
	GameSprite* player2;

	GameSprite* ball;

	Vector<GameSprite*>  players;

	Label* player1ScoreLabel;
	Label* player2ScoreLabel;


	Size screenSize;
	Size courtSize;

	int player1Score;
	int player2Score;

	void playerScore(int player);



public:
	GameLayer();
	virtual ~GameLayer();

	virtual bool init();
	static Scene* scene();

	CREATE_FUNC(GameLayer);

	void onTouchesBegan(const std::vector<Touch*> &touches,
		Event* event);
	void onTouchesMoved(const std::vector<Touch*> &touches,
		Event* event);
	void onTouchesEnded(const std::vector<Touch*> &touches,
		Event* event);
	void update(float dt);


};




#endif // _GAMELAYER_H_